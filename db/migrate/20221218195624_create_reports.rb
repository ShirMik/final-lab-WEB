class CreateReports < ActiveRecord::Migration[7.0]
  def change
    create_table :reports do |t|
      t.string :name, limit: 36
      t.text :description
      t.integer :company_id

      t.timestamps
    end
  end
end
