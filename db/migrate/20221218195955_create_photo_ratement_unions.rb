class CreatePhotoRatementUnions < ActiveRecord::Migration[7.0]
  def change
    create_table :photo_ratement_unions do |t|
      t.integer :photo_id
      t.integer :rate_id
      t.boolean :actual

      t.timestamps
    end
  end
end
