# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: {
    passwords: 'user/passwords',
    registrations: 'user/registrations',
    sessions: 'user/sessions',
    unlocks: 'user/unlocks'
  }
  as :user do
    patch '/users/sign_in', to: 'user/sessions#create'
  end

  get '/good/scan', to: 'goods#barcode'
  get '/good/new', to: 'goods#add'
  post '/good/new', to: 'goods#add'
  post '/good/scan', to: 'goods#process_code', format: 'turbo_stream'
  post '/good/ratement', to: 'goods#ratement'
  post '/good/price', to: 'goods#price'
  get '/user', to: 'cabinet#view'
  get '/me/edit', to: 'cabinet#edit'
  get '/good/:id', to: 'goods#main'

  root 'main#index'
end
