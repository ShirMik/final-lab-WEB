class CreateSubscribments < ActiveRecord::Migration[7.0]
  def change
    create_table :subscribments do |t|
      t.integer :creator_id
      t.integer :subscriber_id
      t.boolean :actual

      t.timestamps
    end
  end
end
