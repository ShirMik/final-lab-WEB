# frozen_string_literal: true

# controller for main page
class MainController < ApplicationController
  layout 'main'

  # GET /
  # def index
  # end
end
