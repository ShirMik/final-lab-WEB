// app/javascript/packs/qr-scan.js
// source: https://dev.to/morinoko/qr-code-reader-on-rails-5816

import { BrowserBarcodeReader } from '@zxing/library';

const codeReader = new BrowserBarcodeReader();

codeReader
  .decodeFromInputVideoDevice(undefined, 'barcam')
  .then((result) => {
    let qrDataFromReader = result.text;

    // Prepare a post request so it can be sent to the Rails controller
    var form = document.getElementById('barform')
    form.barcode.value = qrDataFromReader;

    var submitter = document.getElementById('submitter')
    submitter.click()
  })
  .catch(error => {
    console.error(error);
  });

