# frozen_string_literal: true

# Photo class
class Photo < ApplicationRecord
  has_one_attached :image

  def initialize(*args)
    if %i[submitter_id image].all? { |key| args[0].key? key }
      t_image = MiniMagick::Image.open(args[0][:image].path)
      k = t_image.width.to_f / t_image.height

      t_image.resize "#{512 * k}x512"
      t_image.format 'jpeg' if t_image.mime_type != 'image/jpeg'

      super(submitter: args[0][:submitter_id])

      image.attach(
        io: File.open(t_image.path),
        filename: "#{t_image.signature}.jpeg",
        content_type: t_image.mime_type,
        identify: false
      )
    else
      super
    end
  end
end
