class CreatePrices < ActiveRecord::Migration[7.0]
  def change
    create_table :prices do |t|
      t.integer :value, limit: 100_000_000
      t.integer :good_id
      t.integer :reporter_id

      t.timestamps
    end
  end
end
