class CreatePhotoGoodUnions < ActiveRecord::Migration[7.0]
  def change
    create_table :photo_good_unions do |t|
      t.integer :photo_id
      t.integer :good_id
      t.boolean :actual

      t.timestamps
    end
  end
end
