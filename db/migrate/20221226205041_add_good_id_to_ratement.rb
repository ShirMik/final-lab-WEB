class AddGoodIdToRatement < ActiveRecord::Migration[7.0]
  def change
    add_column :ratements, :good_id, :integer
  end
end
