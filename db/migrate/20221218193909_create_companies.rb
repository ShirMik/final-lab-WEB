class CreateCompanies < ActiveRecord::Migration[7.0]
  def change
    create_table :companies do |t|
      t.string :name, limit: 36
      t.integer :logo_id
      t.text :description

      t.timestamps
    end
  end
end
