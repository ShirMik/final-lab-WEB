class CreateRatements < ActiveRecord::Migration[7.0]
  def change
    create_table :ratements do |t|
      t.integer :rating, limit: 1
      t.string :pros, limit: 512
      t.string :cons, limit: 512
      t.string :other, limit: 512
      t.integer :author_id
      t.boolean :actual

      t.timestamps
    end
  end
end
