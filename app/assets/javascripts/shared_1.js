function hide_all(pattern, val=1) {
  let nodes = document.querySelectorAll(pattern)

  nodes.forEach(function(node){
    switch (val) {
      case 1:
        node.hidden = true;
        break;
      case 0:
        node.hidden = !node.hidden
        break;
      case -1:
        node.hidden = false;
        break;
    }
  });
}

