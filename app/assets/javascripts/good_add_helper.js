function showPreview(event, id) {
  // source: https://dev.to/shantanu_jana/how-to-preview-image-before-uploading-in-javascript-1f6g
  if(event.target.files.length > 0){
    var src = URL.createObjectURL(event.target.files[0]);
    var preview = document.getElementById(id);
    preview.src = src;
    preview.style.display = "block";
  }
}

function selector(event) {
  let widgets = document.querySelectorAll('div.company *')
  let select_widg = document.getElementById('company_id')

  let hidden = (select_widg.selectedIndex != 0)
  widgets.forEach(function(widg){ widg.hidden = hidden })
}

