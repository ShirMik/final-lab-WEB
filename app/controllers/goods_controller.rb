# frozen_string_literal: true

# controller for scan, add and show goods
class GoodsController < ApplicationController
  layout 'main'
  before_action :authenticate_user!, only: %i[add price ratement]

  # GET /good/17
  def main
    if params[:id].to_i.nil?
      redirect_to '/'
    else
      respond_to do |form|
        good_obj = Good.find params[:id].to_i
        form.html { render :good_page, locals: { good: good_obj } }
      end
    end
  end

  # POST /good/ratement
  def ratement
    return unless (params.key? :pros) &&
                  (params.key? :cons) &&
                  (params.key? :other) &&
                  (params.key? :good_id) &&
                  (params.key? :rating)

    rating_val = params[:rating].to_i
    good_id_val = params[:good_id].to_i

    return if rating_val.zero? || good_id_val.zero?

    ratement = Ratement.new pros: params[:pros],
                            cons: params[:cons],
                            other: params[:other],
                            author_id: current_user.id,
                            good_id: good_id_val,
                            rating: rating_val,
                            actual: true
    ratement.save
  end

  # POST /good/price
  def price
    return unless (params.key? :price) && (params.key? :good_id)

    price_val = params[:price].to_i
    good_id_val = params[:good_id].to_i

    return if price_val.zero? || good_id_val.zero?

    price = Price.new value: price_val, good_id: good_id_val, reporter_id: current_user.id
    price.save
  end

  # GET /good/scan
  def barcode() end

  # GET /good/company
  def company() end

  # GET/POST /good/new
  def add
    if params.key? :barcode
      if request.method == 'POST'
        params[:user] = current_user
        good_id = GoodsHelper.new_good(params)

        redirect_to "/good/#{good_id}"
      end
    else
      redirect_to '/good/scan'
    end
  end

  # POST /good/scan
  def process_code
    return unless params.key? :barcode

    puts params
    barcode = params[:barcode]
    codes = Barcode.where(code: barcode)
    goods = codes.collect { |code| Good.where id: code.good_id }
                 .inject([], :+)
    respond_to do |format|
      format.html
      format.turbo_stream { render :goods, locals: { 'goods': goods, 'code': barcode } }
    end
  end
end
