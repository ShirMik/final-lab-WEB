class CreatePhotoReportUnions < ActiveRecord::Migration[7.0]
  def change
    create_table :photo_report_unions do |t|
      t.integer :photo_id
      t.integer :report_id
      t.boolean :actual

      t.timestamps
    end
  end
end
