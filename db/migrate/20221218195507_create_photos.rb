class CreatePhotos < ActiveRecord::Migration[7.0]
  def change
    create_table :photos do |t|
      t.string :path_to_pic
      t.integer :submitter

      t.timestamps
    end
  end
end
