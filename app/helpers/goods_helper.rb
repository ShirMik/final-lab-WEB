# frozen_string_literal: true

module GoodsHelper
  def self.new_good(params)
    user = params[:user]
    form_params = params.permit(:barcode, :name, :descr,
                                :good_photo, :company_id,
                                :comp_name, :comp_descr,
                                :comp_logo)
    puts "Adding good #{form_params[:name]} uploaded by #{user.id}"
    mainpic = Photo.new submitter_id: user.id,
                        image: form_params[:good_photo]
    mainpic.save

    if form_params[:company_id] == ''
      logopic = Photo.new submitter_id: user.id,
                          image: form_params[:comp_logo]
      logopic.save

      company = Company.new name: form_params[:comp_name],
                            logo_id: logopic.id,
                            description: form_params[:comp_descr]
      company.save
      company_id = company.id
    else
      company_id = form_params[:company_id].to_i
    end

    good_params = [form_params[:name], form_params[:descr],
                   company_id, mainpic.id]
    good = Good.new name: good_params[0], description: good_params[1],
                    company_id: good_params[2],
                    main_pic_id: good_params[3]
    good.save

    barcode = Barcode.new code: form_params[:barcode],
                          good_id: good.id
    barcode.save
    good.id
  end
end
